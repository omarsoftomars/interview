Eastern Enterprise Extension
=====================

# Changelog
All notable changes to this project will be documented in this file.
## [1.0.0] - [release date TBD]
### Added
- Initial module definition with dependencies
- New admin config section, group and fields for API (url, user, pass)
- Client Model to call the api endpoint and get the json response with prices for the specified skus
- Plugin on ListProduct block to achieve rendering the external price instead of the one saved on the product
- Plugin on the action Magento\Catalog\Controller\Category\View for category product list pages to set external price on loaded products
- Plugin on Magento\Framework\App\View for search page to set external price on loaded products
- sortorder attribute for plugins defined in Amasty so they are loaded after our plugins
- amscroll-mixin to change the Amasty's scroll to bypass caching and to load the first page with ajax
- css to hide first product page intially loaded
