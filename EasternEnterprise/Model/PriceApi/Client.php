<?php

namespace Interview\EasternEnterprise\Model\PriceApi;

use Interview\EasternEnterprise\Model\Service\Config;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\HTTP\ZendClient;
use Magento\Framework\HTTP\ZendClientFactory;
use Psr\Log\LoggerInterface;
use Zend_Http_Client;
use Exception;

/**
 * Class Client
 */
class Client
{
    /**
     * @var ZendClientFactory
     */
    private $httpClientFactory;

    /**
     * @var Config
     */
    private $configService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var array
     */
    private $skus = [];

    /**
     * Client constructor.
     * @param Config $configService
     * @param ZendClientFactory $httpClientFactory
     * @param LoggerInterface $logger
     */
    public function __construct(Config $configService, ZendClientFactory $httpClientFactory, LoggerInterface $logger)
    {
        $this->configService = $configService;
        $this->httpClientFactory = $httpClientFactory;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public function execute($skus)
    {
        $this->skus = $skus;
        return $this->getResponse();
    }

    /**
     * @param int $retries
     * @return array
     */
    private function getResponse($retries = 0): array
    {
        $response = [];
        $requestUrl = $this->getUrl();
        $params = $this->getRequestParams();

        if (!$requestUrl || !$params) {
            return $response;
        }

        /** @var ZendClient $httpClient */
        $httpClient = $this->httpClientFactory->create();
        if ($this->getHttpMethod() == Zend_Http_Client::GET) {
            $requestUrl .= $this->buildUrlParams($params);
        } else {
            $httpClient->setRawData(json_encode($params));
        }
        try {
            $httpClient->setHeaders($this->getHeaders());
            $httpClient->setUri($requestUrl);
            $httpClient->setConfig(['timeout' => 200]);
            $jsonResponse = $httpClient->request($this->getHttpMethod())->getBody();

            $response = json_decode($jsonResponse, true) ?: [];
        } catch (Exception $exception) {
            if ($retries > 0) {
                $response = $this->getResponse($retries--);
            } else {
                $this->logger->error($exception);
            }
        }
        return $response;
    }

    /**
     * @return array
     */
    private function getRequestParams(): array
    {
        return $this->skus;
    }

    /**
     * @return string
     */
    private function getUrl(): string
    {
        return $this->configService->getApiUrl() ?? '';
    }

    /**
     * @return string
     */
    private function getHttpMethod(): string
    {
        return Zend_Http_Client::GET;
    }

    /**
     * @param $params
     * @return string
     */
    private function buildUrlParams($params): string
    {
        $res = '';
        foreach ($params as $i => $param) {
            $sep = $i == 0 ? '?' : '&';
            $res .= $sep . 'sku[]=' . urlencode($param);

        }
        return $res;
    }

    /**
     * @return array
     */
    private function getHeaders(): array
    {
        return [
            Zend_Http_Client::CONTENT_TYPE => 'application/json',
            'Accept' => 'application/json',
            'username' => $this->configService->getApiUser(),
            'password' => $this->configService->getApiPass()
        ];
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return (bool) $this->getUrl();
    }
}
