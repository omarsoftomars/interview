<?php

namespace Interview\EasternEnterprise\Model\Service;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 */
class Config
{

    const EXTERNAL_DYNAMIC_PRICE_API = 'eastern_enterprise/api/url';
    const EXTERNAL_DYNAMIC_PRICE_USER = 'eastern_enterprise/api/user';
    const EXTERNAL_DYNAMIC_PRICE_PASS = 'eastern_enterprise/api/pass';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     *
     * @param string $scope
     * @param null|int|string $scopeCode
     * @return null|stringl
     */
    public function getApiUrl($scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): ?string
    {
        return $this->scopeConfig->getValue(self::EXTERNAL_DYNAMIC_PRICE_API, $scope, $scopeCode);
    }

    /**
     * @param $scope
     * @param null $scopeCode
     * @return string|null
     */
    public function getApiUser($scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): ?string
    {
        return $this->scopeConfig->getValue(self::EXTERNAL_DYNAMIC_PRICE_USER, $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param null|int|string $scopeCode
     * @return null|string
     */
    public function getApiPass($scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): ?string
    {
        return $this->scopeConfig->getValue(self::EXTERNAL_DYNAMIC_PRICE_PASS, $scope, $scopeCode);
    }
}
