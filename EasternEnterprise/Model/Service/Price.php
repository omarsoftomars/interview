<?php

namespace Interview\EasternEnterprise\Model\Service;

use Amasty\Scroll\Helper\Data as ScrollService;
use Interview\EasternEnterprise\Model\PriceApi\Client;
use Exception;

/**
 * Class Price
 */
class Price
{
    /**
     * @var ScrollService
     */
    private $scrollConfig;

    /**
     * @var Client
     */
    private $client;

    /**
     * Price constructor.
     * @param Client $client
     */
    public function __construct(Client $client, ScrollService $scrollConfig)
    {
        $this->client = $client;
        $this->scrollConfig = $scrollConfig;
    }

    /**
     * Applies the API prices on the products from the input page only when API and scroll features are enabled
     *
     * @param $page
     * @throws Exception
     */
    public function applyOnProductList($page)
    {
        if (!$this->client->isEnabled() || !$this->scrollConfig->isEnabled()) {
            return;
        }

        $products = $page->getLayout()->getBlock('category.products.list');
        if (!$products) {
            $products = $page->getLayout()->getBlock('search_result_list');
        }

        $collection = $products->getLoadedProductCollection();
        $skus = $collection->getColumnValues('sku');

        $response = $this->client->execute($skus);
        foreach ($collection as $product) {
            if (isset($response[$product->getSku()])) {
                $product->setExternalPrice($response[$product->getSku()]);
            }
        }
    }
}
