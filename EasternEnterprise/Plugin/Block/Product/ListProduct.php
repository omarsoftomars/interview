<?php

namespace Interview\EasternEnterprise\Plugin\Block\Product;

use Magento\Catalog\Model\Product;
use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Class ListProduct
 */
class ListProduct
{
    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * ListProduct constructor.
     */
    public function __construct(PriceCurrencyInterface $priceCurrency)
    {
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * This plugin is to render product's external price instead of the saved one
     *
     * @param $subject
     * @param $result
     * @param Product $product
     * @return string
     */
    public function afterGetProductPrice($subject, $result, Product $product)
    {
        if ($price = $product->getExternalPrice()) {
            return '<div id="external-price-' . $product->getId() . '">' . $this->priceCurrency->format($price) . '</div>';
        }
        return $result;
    }
}
