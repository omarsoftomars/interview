<?php

namespace Interview\EasternEnterprise\Plugin\Catalog\Controller\Category;

use Magento\Catalog\Controller\Category\View as MagentoView;
use Magento\Framework\View\Result\Page;
use Magento\Framework\Controller\Result\Raw;
use Interview\EasternEnterprise\Model\Service\Price;

/**
 * Class View
 */
class View
{
    /**
     * @var Price
     */
    private $priceService;

    /**
     * View constructor.
     * @param Price $priceService
     */
    public function __construct(Price $priceService)
    {
        $this->priceService = $priceService;
    }

    /**
     * Get the external prices list for this page and apply them on the products
     *
     * @param MagentoView $controller
     * @param Page $page
     * @return Page|null
     */
    public function afterExecute($controller, $page = null)
    {
        if ($page instanceof Page) {
            $this->priceService->applyOnProductList($page);
        }

        return $page;
    }
}
