<?php

namespace Interview\EasternEnterprise\Plugin\Framework\App;

use Magento\Framework\App\View as MagentoView;
use Magento\Framework\View\Result\Page;
use Interview\EasternEnterprise\Model\Service\Price;

/**
 * Class View
 */
class View
{
    /**
     * @var Price
     */
    private $priceService;

    /**
     * View constructor.
     * @param Price $priceService
     */
    public function __construct(Price $priceService)
    {
        $this->priceService = $priceService;
    }

    /**
     * Get the external prices list for this page and apply them on the products
     *
     * @param MagentoView $subject
     * @param \Closure $proceed
     * @param string $output
     * @return mixed
     */
    public function beforeRenderLayout(MagentoView $subject, $output = '')
    {
        $page = $subject->getPage();

        if ($page instanceof Page) {
            $this->priceService->applyOnProductList($page);
        }
    }
}
