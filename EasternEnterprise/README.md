Eastern Enterprise Extension
=====================
## Installation
- Manual installaion: 

    - [Download Amasty modules](https://drive.google.com/drive/folders/1yPOqLghCl0eZ_51jNmVAjATO8TPwdbcX)
    - clone this repository
    - add both vendors' modules in app/code/
    - run bin/magento setup:upgrade

- Install with composer make sure you have the needed Amasty modules and the vcs repository for this module
  ```
    composer require interview/easternenterprise
  ```
## Configuration
- Used to load dynamically the prices of the products from an external system
- Depends on Amasty Scroll, the mode should be button or automatic otherwise the dynamic prices API will not dynamically change the prices
- Configure API from Stores > Configuration > Eastern Enterprise for now only API url is used, leave blank to disable and so you can keep the infinite scroll with original pricing
- The response of the pricing system should be of type json and of the following format:
{"sku1" : 111.12, "sku2" : 55.45, ...}
- The call looks as follows: configuredUrl?sku[]=MT12&sku[]=MT11&sku[]=MT10&..
- The below code can be used for testing purpose in order to randomly generate prices, feel free to change the randomization function (mt_rand, shuffle, etc..)
```
    <?php
    
    $skus= $_GET['sku'] ?? [];
    $rsp = [];
    foreach ($skus as $sku)
    {
        $rsp[$sku] = rand(0, 500) / 10;
    }
    echo json_encode($rsp);
```
- Control pagination from Products per Page in Stores > Configuration > Catalog > Catalog > Storefront
- Works only on product list pages (category view, search)

## Demo
- Find this [demo frontend](https://magento-414206-1688524.cloudwaysapps.com/women/tops-women.html) where you can access 
[backend](https://magento-414206-1688524.cloudwaysapps.com/admin/) with sser: omar pass: aqswde123
- Enjoy this [demo video](https://drive.google.com/drive/folders/1yPOqLghCl0eZ_51jNmVAjATO8TPwdbcX)

## Known issues and future improvements

- As this module was requested to affect only product list pages show the external prices dynamically it will be easy to do the same for single product pages and everywhere final price is displayed
- The checkout, add to cart and order flows can be adapted to use the dynamic pricing
- Needs to be analysed for promotions, catalog price rules, bundle pricing, etc..
- API authorization
- Configuration can be improved with backend model and dependency on Amasty's toggle config. [Magento bug for dependency between different sections](https://github.com/magento/magento2/issues/24125)
  