define([
    "jquery"
], function ($) {
    "use strict";

    return function (targetWidget) {
        $.widget('mage.amScrollScript', targetWidget, {
            initialize: function () {
                this.next_data_cache = "";
                this.pagesLoaded = [];
                this._initPagesCount();
                this.disabled = 1;

                var isValidConfiguration = this._validate();
                if (!isValidConfiguration) {
                    $('.amscroll-navbar').remove(); //remove old nav bar
                    return;
                }
                this.disabled = 0;
                this.type = this.options['actionMode'];

                this._preloadPages();
                this._hideToolbars();

                var self = this;
                $(document).ready(function () {
                    var productContainer = $(self.options['product_container']);
                    var pn = productContainer.attr('amscroll-page') || 1;
                    productContainer.html('');
                    $.getJSON(self._generateUrl(pn, 1), {_: new Date().getTime()}, function (data) {
                        productContainer.html('');
                        self._insertNewProductBlock(data, 'after');
                    });
                });


                var self = this;
                $(window).scroll(function () {
                    self._initPaginator();
                });
                setTimeout(function () {
                    self._initPaginator();
                }, 7000);

                this._initProgressBar();

                if (this.type == 'button') {
                    this._generateButton('before', false);
                    this._generateButton('after', false);
                }

                this._pagePositionAfterStepBack();
            },

            _preloadPageAfter: function (page) {
                var nextPage = page + 1;
                if (nextPage && nextPage <= this.pagesCount) {
                    this.next_data_url = this._generateUrl(nextPage, 1);
                    this.pagesLoaded.push(nextPage);
                    var self = this;
                    self.flag_next_cache = 1;

                    $.getJSON(this.next_data_url, {_: new Date().getTime()}, function (data) {
                        if (data.categoryProducts) {
                            self.flag_next_cache = 0;
                            self.next_data_cache = data;
                        } else {
                            self._stop();
                        }
                    }).fail(function () {
                        self._stop();
                    });

                    this.next_data_url = '';
                }
            },

            _preloadPageBefore: function (page) {
                var prevPage = page - 1;
                if (prevPage && prevPage >= 1) {
                    this.prev_data_url = this._generateUrl(prevPage, 1);
                    this.pagesLoaded.unshift(prevPage);
                    var self = this;
                    self.flag_prev_cache = 1;

                    $.getJSON(this.prev_data_url, {_: new Date().getTime()}, function (data) {
                        if (data.categoryProducts) {
                            self.flag_prev_cache = 0;
                            self.prev_data_cache = data;
                        } else {
                            self._stop();
                        }
                    }).fail(function () {
                        self._stop();
                    });

                    this.prev_data_url = '';
                }
            },

            _loadFollowing: function () {
                var self = this;
                if (this.flag_next_cache && this.type != 'button') {
                    this._createLoading('after');
                }
                if (this.next_data_url != "" || this.next_data_cache) {
                    if (this.type != 'button') {
                        this._createLoading('after');
                    }
                    if (this.next_data_cache) {
                        this.showFollowing(this.next_data_cache);
                    } else {
                        if (!this.flag_next_cache) {
                            this.is_loading = 1; // note: this will break when the server doesn't respond
                            $.getJSON(this.next_data_url, {_: new Date().getTime()}, function (data) {
                                self.showFollowing(data);
                            });
                        }
                    }
                }
            },

            _afterShowFollowing: function () {
                var nextPage = $(this.pagesLoaded).get(-1) + 1;//last + 1
                if (nextPage && nextPage <= this.pagesCount && $.inArray(nextPage, this.pagesLoaded) == -1) {
                    this.next_data_url = this._generateUrl(nextPage, 1);
                    this.pagesLoaded.push(nextPage);
                    var self = this;
                    this.flag_next_cache = 1;
                    $.getJSON(this.next_data_url, {_: new Date().getTime()}, function (preview_data) {
                        self.flag_next_cache = 0;
                        self.next_data_cache = preview_data;
                        $(window).scroll();
                    });
                }

                this.is_loading = 0;
            },

            _loadPrevious: function () {
                var self = this;
                if (this.flag_prev_cache && this.type != 'button') {
                    this._createLoading('before');
                }
                if (this.prev_data_url != "" || this.prev_data_cache) {
                    if (this.type != 'button') {
                        this._createLoading('before');
                    }
                    if (this.prev_data_cache) {
                        this.showPrevious(this.prev_data_cache);
                    } else {
                        if (!this.flag_prev_cache) {
                            this.is_loading = 1; // note: this will break when the server doesn't respond
                            $.getJSON(this.prev_data_url, {_: new Date().getTime()},  function (data) {
                                self.showPrevious(data);
                            });
                        }
                    }
                }
            },

            _afterShowPrevious: function () {
                var prevPage = $(this.pagesLoaded).get(0) - 1;
                if (prevPage && prevPage >= 1 && $.inArray(prevPage, this.pagesLoaded) == -1) {
                    this.prev_data_url = this._generateUrl(prevPage, 1);
                    this.pagesLoaded.unshift(prevPage);
                    var self = this;
                    this.flag_prev_cache = 1;

                    $.getJSON(this.prev_data_url, {_: new Date().getTime()}, function (preview_data) {
                        self.flag_prev_cache = 0;
                        self.prev_data_cache = preview_data;
                        $(window).scroll();
                    });
                }
                this.is_loading = 0;
            },

        });

        return $.mage.amScrollScript;
    };
});